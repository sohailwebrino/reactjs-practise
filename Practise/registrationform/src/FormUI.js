import React from "react"

function Form(props) {
    return (
        <main>
            <form style={{}}>
                <h4>Name:</h4> 
                <input 
                    name="Name" 
                    value={props.data.Name} 
                    onChange={props.handleChange} 
                    placeholder="Name" 
                />
                <h4>Age:</h4> 
                <input 
                    name="age" 
                    value={props.data.age}
                    onChange={props.handleChange} 
                    placeholder="Enter your Age" 
                />
                <h4>Gender:</h4> 
                <label>
                    <input 
                        type="radio" 
                        name="gender"
                        value="male"
                        checked={props.data.gender === "male"}
                        onChange={props.handleChange}
                    /> Male
                </label>
                
                <label>
                    <input 
                        type="radio" 
                        name="gender"
                        value="female"
                        checked={props.data.gender === "female"}
                        onChange={props.handleChange}
                    /> Female
                </label>
                <h4>Status:</h4> 
                <select 
                    value={props.data.status} 
                    name="status" 
                    onChange={props.handleChange}
                >
                    <option value="">choose your marital status</option>
                    <option value="single">Single</option>
                    <option value="married">Married</option>
                    <option value="divorsed">Divorsed</option>
                </select>
                <h4>Gadgets Own:</h4> 
                <label>
                    <input 
                        type="checkbox"
                        name="smartphone"
                        onChange={props.handleChange}
                        checked={props.data.smartphone}
                    /> Smartphone
                </label>
                
                <label>
                    <input 
                        type="checkbox"
                        name="laptop"
                        onChange={props.handleChange}
                        checked={props.data.laptop}
                    /> Laptop
                </label>
                
                <label>
                    <input 
                        type="checkbox"
                        name="tablet"
                        onChange={props.handleChange}
                        checked={props.data.tablet}
                    /> Tablet
                </label>
                <br/>
                <br/>
                <button>Submit</button>
            </form>
            <div style={{marginTop:"-386px",textAlign:"center"}}>
            <h2>Personal Details</h2>
            <p>Your Name: {props.data.Name}</p>
            <p>Your Age: {props.data.age}</p>
            <p>Your Gender: {props.data.gender}</p>
            <p>Your Status: {props.data.status}</p>
            <p>Your Gadgets:</p>
            
            <p>Smartphone: {props.data.smartphone ? "Yes" : "No"}</p>
            <p>Laptop: {props.data.laptop ? "Yes" : "No"}</p>
            <p>Tablet: {props.data.tablet ? "Yes" : "No"}</p>
            </div>
        </main>
    )
}

export default Form