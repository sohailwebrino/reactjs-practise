import React, {Component} from "react"
import FormUI from "./FormUI"

class FormLogic extends Component {
    constructor() {
        super()
        this.state = {
            Name: "",
            age: "",
            gender: "",
            status: "",
            smartphone: false,
            laptop: false,
            tablet: false
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        const {name, value, type, checked} = event.target
        type === "checkbox" ? 
            this.setState({
                [name]: checked
            })
        :
        this.setState({
            [name]: value
        }) 
    }
    
    render() {
        return(
            <FormUI
                handleChange={this.handleChange}
                data={this.state}
            />
        )
    }
}

export default FormLogic
