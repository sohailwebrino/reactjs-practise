import React from "react"
class Header extends React.Component {
    render() {
        return (
            <header>
                <p style={{fontSize:"20px"}}>Welcome to the webApp, {this.props.username}!</p>
            </header>
        )    
    }
}

export default Header