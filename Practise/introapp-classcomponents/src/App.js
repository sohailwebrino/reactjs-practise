import React from "react"
import Header from "./Header"
import Content from "./Content"
class App extends React.Component {
  render() {
      return (
          <div>
              <Header username="Sohail"/>
              <Content />
          </div>
      )    
  }
}

export default App