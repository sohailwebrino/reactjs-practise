import React, {Component} from "react"

class Content extends Component {
    render() {
        let url1="https://www.linkedin.com/in/sohailhussain5/";
        let url2="https://github.com/sohailhussain5";
        const date = new Date()
        const hours = date.getHours()
        let timeOfDay
        
        if (hours < 12) {
            timeOfDay = "Good morning, have a nice day"
        } else if (hours >= 12 && hours < 17) {
            timeOfDay = "Good afternoon, have a good day ahead"
        } else {
            timeOfDay = "Good night, Sweet dreams"
        }
        return (
            <div>
            <h1>{timeOfDay}</h1>
            <p style={{fontSize:"25px", backgroundColor:"lightyellow"}}>A place where you will find everything you like, amazing contents and stuff.</p>
            <ul>
                <li><a href={url1}>LinkedIn</a></li>
                <li><a href={url2}>Github</a></li>
            </ul>
            </div>
        )
    }
}

export default Content