import React from "react"
import randomcolor from "randomcolor"
class App extends React.Component {
    constructor() {
        super()
        this.state = {
            isLoggedIn: false,
            count: 24,
            color: "red",
            name: "Sohail",
            age: 24
        }
        this.handleClick = this.handleClick.bind(this)
        this.increment = this.increment.bind(this)
        this.decrement = this.decrement.bind(this)
    }
    handleClick() {
        this.setState(prevState => {
            return {
                isLoggedIn: !prevState.isLoggedIn
            }
        })
    }
    increment() {
      this.setState(prevState => {
          return {
              count: prevState.count + 10
          }
      })
  }
  decrement() {
      this.setState(prevState => {
          return {
              count: prevState.count - 10
          }
      })
  }
  componentDidUpdate(prevProps, prevState) {
    if(prevState.count !== this.state.count) {
        const newColor = randomcolor()
        this.setState({color: newColor})
    }
}
    render() {   
        let buttonText = this.state.isLoggedIn ? "LOG OUT" : "LOG IN"
        let displayText = this.state.isLoggedIn ? "Hey Sohail You are Logged in" : "Bye Sohail,You are Logged out, Thank you"
        return (
            <div>
              <h1>Hey, I'm {this.state.name}</h1>
              <h3>and I'm {this.state.age} years old</h3>
              <h1 style={{color: this.state.color}}>{this.state.count}</h1>
              <button style={{color: this.state.color}} onClick={this.increment}>After 10 years i will be
              </button>
              <br />
              <button style={{color: this.state.color}} onClick={this.decrement}>10 years back i was
              </button>
              <h1>{displayText}</h1>
              <button style={{color:"white",backgroundColor:"blue",}} onClick={this.handleClick}>{buttonText}</button>
            </div>
        )
    }
}

export default App
