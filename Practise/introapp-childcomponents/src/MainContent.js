import React from "react"

function MainContent(){
    let url1="https://www.linkedin.com/in/sohailhussain5/";
    let url2="https://github.com/sohailhussain5";
    return(
        <main>
        <p style={{fontSize:"25px", backgroundColor:"lightyellow"}}>A place where you will find everything you like, amazing contents and stuff.</p>
            <ul>
                <li><a href={url1}>LinkedIn</a></li>
                <li><a href={url2}>Github</a></li>
            </ul>
        </main>
    )
}

export default MainContent