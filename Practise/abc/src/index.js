import React from "react"
import ReactDOM from "react-dom"

function App() {
  const date = new Date()
  const hours = date.getHours()
  let timeOfDay
  const styles = {
    fontSize: 20
  }
  
  if (hours < 12) {
    timeOfDay = "Good Morning Have a nice day"
    styles.color = "Orange"
  } else if (hours >= 12 && hours < 17) {
    timeOfDay = "Good Afternoon Enjoy your day"
    styles.color = "Red"
  } else {
    timeOfDay = "Good Night Sweet Dreams"
    styles.color = "Green"
  }
  
  return (
    <div>
      <h1 style={{color:"Red"}}> SHS </h1>
      <p style={{color:"orange"}}>Iam from udaipur city</p>
      <ol>
        <li>Shimla</li>
        <li>Manali</li>
        <li>Kashmir</li>
      </ol>
      <h1 style={styles}> {timeOfDay}</h1>
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById("root"))